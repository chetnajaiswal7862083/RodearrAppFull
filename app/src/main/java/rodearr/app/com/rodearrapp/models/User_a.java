package rodearr.app.com.rodearrapp.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by wel come on 08-06-2018.
 */

public class User_a {
    @SerializedName("username")
    private String username;


    @SerializedName("role")
    private String role;

    public User_a(String username, String role) {
        this.username = username;
        this.role = role;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }


    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
