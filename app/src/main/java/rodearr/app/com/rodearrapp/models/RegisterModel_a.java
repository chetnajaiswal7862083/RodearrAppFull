package rodearr.app.com.rodearrapp.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by wel come on 08-06-2018.
 */

public class RegisterModel_a {
    @SerializedName("id")
    private String id;

    @SerializedName("firstname")
    private String firstname;

    @SerializedName("lastname")
    private String lastname;

    @SerializedName("gender")
    private String gender;

    @SerializedName("contact_number")
    private String contact_number;

    @SerializedName("dob")
    private String dob;

    @SerializedName("license_number")
    private String license_number;

    @SerializedName("address")
    private Address_a address;
    @SerializedName("user")
    private User_a user;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getContact_number() {
        return contact_number;
    }

    public void setContact_number(String contact_number) {
        this.contact_number = contact_number;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getLicense_number() {
        return license_number;
    }

    public void setLicense_number(String license_number) {
        this.license_number = license_number;
    }

    public Address_a getAddress() {
        return address;
    }

    public void setAddress(Address_a address) {
        this.address = address;
    }

    public User_a getUser() {
        return user;
    }

    public void setUser(User_a user) {
        this.user = user;
    }
}
