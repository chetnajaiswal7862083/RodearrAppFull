package rodearr.app.com.rodearrapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;


import java.util.ArrayList;
import java.util.List;


import rodearr.app.com.rodearrapp.R;
import rodearr.app.com.rodearrapp.models.LanguageListModel;

/**
 * Created by wel come on 07-06-2018.
 */

public class LanguageAdapter extends RecyclerView.Adapter<LanguageAdapter.MyViewHolder> {
    private List<LanguageListModel> moviesList;
    private ArrayList<Integer> IMAGES;
    Context mContext;
    private ArrayList<String> nameList;
    private ArrayList<String> movie_name;

    private int lastSelectedPosition = -1;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name, year, genre, new_textview, votes, rating, types;
        ImageView image;
        RadioButton selectionState;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            selectionState = (RadioButton) itemView.findViewById(R.id.langRadio);
           selectionState.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    lastSelectedPosition = getAdapterPosition();
                    notifyDataSetChanged();


                }
            });
        }
    }


    public LanguageAdapter(ArrayList<String> nameList) {

        this.nameList=nameList;


    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.languagelistitem, parent, false);



        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        //for default check in first item
        holder.selectionState.setChecked(lastSelectedPosition == position);

        holder.name.setText(nameList.get(position));

       //  holder.image.setImageResource(IMAGES.get(position));
       /* Movie movie = moviesList.get(position);

        holder.genre.setText(movie.getGenre());
        holder.year.setText(movie.getYear());*/
    }

    @Override
    public int getItemCount() {
        return nameList.size();
    }
}
