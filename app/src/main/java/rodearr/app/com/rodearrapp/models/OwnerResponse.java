package rodearr.app.com.rodearrapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by wel come on 06-07-2018.
 */

public class OwnerResponse {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("contact_number")
    @Expose
    private String contact_number;

    @SerializedName("user")
    @Expose
    private User_a user;

    public User_a getUser() {
        return user;
    }

    public void setUser(User_a user) {
        this.user = user;
    }

    public OwnerResponse(Integer id, String firstname, String lastname, String contact_number, User_a user) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.contact_number = contact_number;
        this.user = user;
    }

    public String getContact_number() {
        return contact_number;
    }

    public void setContact_number(String contact_number) {
        this.contact_number = contact_number;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    @Override
    public String toString() {
        return firstname+" "+lastname+"("+user.getUsername()+")";
    }




}
