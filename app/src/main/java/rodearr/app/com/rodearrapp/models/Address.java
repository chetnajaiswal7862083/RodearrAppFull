package rodearr.app.com.rodearrapp.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by wel come on 08-06-2018.
 */

public class Address {

    @SerializedName("house")
    private String house;

    @SerializedName("address1")
    private String address1;

    @SerializedName("nearby")
    private String nearby;

    @SerializedName("area")
    private int area;

    @SerializedName("city")
    private int city;

    @SerializedName("state")
    private int state;

    @SerializedName("country")
    private int country;

    public Address(String house, String address1, String nearby, int area, int city, int state, int country) {
        this.house = house;
        this.address1 = address1;
        this.nearby = nearby;
        this.area = area;
        this.city = city;
        this.state = state;
        this.country = country;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getNearby() {
        return nearby;
    }

    public void setNearby(String nearby) {
        this.nearby = nearby;
    }

    public int getArea() {
        return area;
    }

    public void setArea(int area) {
        this.area = area;
    }

    public int getCity() {
        return city;
    }

    public void setCity(int city) {
        this.city = city;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }

    public int getCountry() {
        return country;
    }

    public void setCountry(int country) {
        this.country = country;
    }

}
