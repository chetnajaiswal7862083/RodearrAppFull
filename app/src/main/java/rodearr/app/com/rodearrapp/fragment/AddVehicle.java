package rodearr.app.com.rodearrapp.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import rodearr.app.com.rodearrapp.R;
import rodearr.app.com.rodearrapp.global_supporting_items.Global_Data;
import rodearr.app.com.rodearrapp.jsonparser.JSONParser;
import rodearr.app.com.rodearrapp.models.BrandListResponse;
import rodearr.app.com.rodearrapp.models.CategoryListResponse;
import rodearr.app.com.rodearrapp.models.OwnerResponse;
import rodearr.app.com.rodearrapp.models.VehicleListResponse;
import rodearr.app.com.rodearrapp.sharedpreference.SharedPref;


public class AddVehicle extends Fragment {
    private View view;

    @BindViews({R.id.brand, R.id.model, R.id.category, R.id.vehicle_number})
    List<Spinner> spinnerViewList;
    List<BrandListResponse> brandListResponseList;
    List<BrandListResponse.VehicleModel> vehicleModelList;
    List<BrandListResponse.Category> categoryList;
    List<CategoryListResponse> categoryListResponseList;
    List<CategoryListResponse> modelListResponseList;
    List<OwnerResponse> ownerResponseList;
    List<OwnerResponse> driverResponseList;
    //  List<CategoryListResponse> modelListResponseList;
    @BindView(R.id.carNumberEditText)
    EditText carNumberEditText;
    @BindViews({R.id.change_frontsideview_np_layout, R.id.change_backsideview_np_layout, R.id.change_leftsideview_np_layout, R.id.upload_leftsideview_layout, R.id.upload_vehicle_tax_doc_frontview_layout,
            R.id.upload_vehicle_tax_doc_back_layout, R.id.upload_vehicle_rc_card_front_layout, R.id.upload_vehicle_rc_card_back_layout, R.id.vehicle_list_layout})
    List<LinearLayout> linearLayoutsViewList;

    @BindViews({R.id.change_frontsideview_np, R.id.change_backsideview_np, R.id.change_leftsideview_np, R.id.upload_leftsideview, R.id.upload_rightsideview, R.id.upload_vehicle_tax_doc_back,
            R.id.upload_vehicle_rc_card_front, R.id.upload_vehicle_rc_card_back})
    List<Button> btnViewList;

    @BindViews({R.id.frontsideviewnp_imageview, R.id.backsideviewnp_imageview, R.id.leftsideview_imageview, R.id.rightsideview_imageview, R.id.Vehicle_tax_doc_frontview_imageview, R.id.vehicle_tax_doc_back_imageview, R.id.vehicle_rc_card_front_imageview, R.id.vehicle_rc_card_back_imageview})
    List<ImageView> imageViewList;
    private ArrayAdapter<BrandListResponse> brandListResponseArrayAdapter;
    private ArrayAdapter<CategoryListResponse> categoryListResponseArrayAdapter;
    private ArrayAdapter<CategoryListResponse> modelListResponseArrayAdapter;
    private ArrayAdapter<VehicleListResponse> vehicleListResponseArrayAdapter;
    public static final String TAG = "Upload Image";
    private Bitmap bitmap = null, frontsideviewnp_imageview_bitmap = null, backsideviewnp_imageview_bitmap = null, leftsideview_imageview_bitmap = null, rightsideview_imageview_bitmap = null, Vehicle_tax_doc_frontview_imageview_bitmap = null,
            vehicle_tax_doc_back_imageview_bitmap = null, vehicle_rc_card_front_imageview_bitmap = null, vehicle_rc_card_back_imageview_bitmap = null;

    SharedPref _sharedPref;
    private Uri filePath;
    private String selectedFilePath;
    private int PICK_IMAGE_REQUEST = 100;
    private int imageType = 9;

    @BindView(R.id.add_vehicle)
    Button add_vehicle;


    public AddVehicle() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_add_vehicle, container, false);
        ButterKnife.bind(this, view);
        _sharedPref = new SharedPref(getActivity());
        brandListResponseList = new ArrayList<>();
        vehicleModelList = new ArrayList<>();
        categoryList = new ArrayList<>();
        categoryListResponseList = new ArrayList<>();
        modelListResponseList = new ArrayList<>();
        // vehicleListResponseList = new ArrayList<>();
        ownerResponseList = new ArrayList<>();
        driverResponseList = new ArrayList<>();
        brandListResponseArrayAdapter = new ArrayAdapter<BrandListResponse>
                (getActivity(), android.R.layout.simple_spinner_item,
                        brandListResponseList); //selected item will look like a spinner set from XML
        brandListResponseArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        spinnerViewList.get(0).setAdapter(brandListResponseArrayAdapter);

        categoryListResponseArrayAdapter = new ArrayAdapter<CategoryListResponse>
                (getActivity(), android.R.layout.simple_spinner_item,
                        categoryListResponseList); //selected item will look like a spinner set from XML
        categoryListResponseArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        spinnerViewList.get(2).setAdapter(categoryListResponseArrayAdapter);

        modelListResponseArrayAdapter = new ArrayAdapter<CategoryListResponse>
                (getActivity(), android.R.layout.simple_spinner_item,
                        modelListResponseList); //selected item will look like a spinner set from XML
        modelListResponseArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);

        spinnerViewList.get(1).setAdapter(modelListResponseArrayAdapter);
        if (Global_Data.checkInternetConnection(getActivity())) {
            if (Build.VERSION.SDK_INT > 11) {
                new GetBrandList().executeOnExecutor(Global_Data.sExecutor);
                new GetCategory().executeOnExecutor(Global_Data.sExecutor);

            } else {
                new GetBrandList().execute();
                new GetCategory().execute();
            }

        } else {

            Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
        }



        add_vehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Global_Data.checkInternetConnection(getActivity())) {
                    if (Build.VERSION.SDK_INT > 11) {

                        new AddVehicles().executeOnExecutor(Global_Data.sExecutor);
                    } else {

                        new AddVehicles().execute();
                    }

                } else {

                    Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
            }
        });
        spinnerViewList.get(2).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("Item selecting.....   ");
                if (Global_Data.checkInternetConnection(getActivity())) {
                    if (Build.VERSION.SDK_INT > 11) {

                        new GetVehicleModel().executeOnExecutor(Global_Data.sExecutor);
                    } else {

                        new GetVehicleModel().execute();
                    }

                } else {

                    Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerViewList.get(0).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                System.out.println("Item selecting.....   ");
                if (Global_Data.checkInternetConnection(getActivity())) {
                    if (Build.VERSION.SDK_INT > 11) {
                        new GetCategory().executeOnExecutor(Global_Data.sExecutor);
                        new GetVehicleModel().executeOnExecutor(Global_Data.sExecutor);
                    } else {
                        new GetCategory().execute();
                        new GetVehicleModel().execute();
                    }

                } else {

                    Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        linearLayoutsViewList.get(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                imageType = 0;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });

        linearLayoutsViewList.get(1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 1;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });

        linearLayoutsViewList.get(2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 2;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });

        linearLayoutsViewList.get(3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 3;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });

        linearLayoutsViewList.get(4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 4;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });

        linearLayoutsViewList.get(5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 5;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });

        linearLayoutsViewList.get(6).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 6;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });

        linearLayoutsViewList.get(7).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 7;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });
//....................................................................

        btnViewList.get(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 0;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });

        btnViewList.get(1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 1;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });

        btnViewList.get(2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 2;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });

        btnViewList.get(3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 3;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });

        btnViewList.get(4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 4;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });

        btnViewList.get(5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 5;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });

        btnViewList.get(6).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 6;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });

        btnViewList.get(7).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 7;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });

        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {

            filePath = data.getData();
            selectedFilePath = getPath(filePath);
            Log.i(TAG, " File path : " + filePath);
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);
                persistImage(bitmap, "imagefile");

   /*
   frontsideviewnp_imageview_bitmap = null, backsideviewnp_imageview_bitmap = null, leftsideview_imageview_bitmap = null, rightsideview_imageview_bitmap = null, Vehicle_tax_doc_frontview_imageview_bitmap = null,
            vehicle_tax_doc_back_imageview_bitmap = null, vehicle_rc_card_front_imageview_bitmap = null, vehicle_rc_card_back_imageview_bitmap = null;
*/

                if (imageType == 0) {
                    frontsideviewnp_imageview_bitmap = bitmap;
                    imageViewList.get(0).setImageBitmap(bitmap);
                } else if (imageType == 1) {
                    backsideviewnp_imageview_bitmap = bitmap;
                    imageViewList.get(1).setImageBitmap(bitmap);
                } else if (imageType == 2) {
                    leftsideview_imageview_bitmap = bitmap;
                    imageViewList.get(2).setImageBitmap(bitmap);
                } else if (imageType == 3) {
                    rightsideview_imageview_bitmap = bitmap;
                    imageViewList.get(3).setImageBitmap(bitmap);

                } else if (imageType == 4) {
                    Vehicle_tax_doc_frontview_imageview_bitmap = bitmap;
                    imageViewList.get(4).setImageBitmap(bitmap);
                } else if (imageType == 5) {
                    vehicle_tax_doc_back_imageview_bitmap = bitmap;
                    imageViewList.get(5).setImageBitmap(bitmap);
                } else if (imageType == 6) {
                    vehicle_rc_card_front_imageview_bitmap = bitmap;
                    imageViewList.get(6).setImageBitmap(bitmap);
                } else if (imageType == 7) {
                    vehicle_rc_card_back_imageview_bitmap = bitmap;
                    imageViewList.get(7).setImageBitmap(bitmap);
                }
                // imgView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private String persistImage(Bitmap bitmap, String name) {
        File filesDir = getActivity().getFilesDir();
        File imageFile = new File(filesDir, name + ".jpg");
        String file_String = "";
        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            System.out.println("OS---   " + imageFile.getName());
            file_String = imageFile.toString();
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }
        return file_String;
    }

    private class GetBrandList extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(getActivity());


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                jsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "brands/", "GET", json2);
                System.out.println("Brand response in vehicle---   " + jsonArray.length());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                for (int i = 0; i < jsonArray.length(); i++) {
                    brandListResponseList.add(new BrandListResponse(jsonArray.getJSONObject(i).getInt("id"),
                            jsonArray.getJSONObject(i).getString("name").trim(), jsonArray.getJSONObject(i).getString("name"), vehicleModelList));


                }

                brandListResponseArrayAdapter.notifyDataSetChanged();


                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    //............................................................................................

    private class GetCategory extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(getActivity());


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                jsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "categories/", "GET", json2);
                System.out.println("Category response in vehicle---   " + jsonArray.length());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                categoryListResponseList.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                 //   System.out.println("JSONARRAY-----   " + jsonArray.getJSONObject(i).getJSONArray("brands"));
                    for (int j = 0; j < jsonArray.getJSONObject(i).getJSONArray("brands").length(); j++) {
                     //   System.out.println("BRAND  JSONARRAY-----   " + jsonArray.getJSONObject(i).getJSONArray("brands").getJSONObject(j).get("name"));
                     //   System.out.println("Selected brand---   " + brandListResponseList.get(spinnerViewList.get(0).getSelectedItemPosition()));
                        if (brandListResponseList.get(spinnerViewList.get(0).getSelectedItemPosition()).toString().trim().equals(jsonArray.getJSONObject(i).getJSONArray("brands").getJSONObject(j).getString("name").trim())) {
                            categoryListResponseList.add(new CategoryListResponse(Integer.parseInt(jsonArray.getJSONObject(i).getString("id")), jsonArray.getJSONObject(i).getString("name"), jsonArray.getJSONObject(i).getString("desc")));

                        }
                    }

                }

                categoryListResponseArrayAdapter.notifyDataSetChanged();
                progressDialog.dismiss();
                if (Global_Data.checkInternetConnection(getActivity())) {
                    if (Build.VERSION.SDK_INT > 11) {

                        new GetVehicleModel().executeOnExecutor(Global_Data.sExecutor);
                    } else {

                        new GetVehicleModel().execute();
                    }

                } else {

                    Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
            } catch (Exception e) {
                progressDialog.dismiss();
                e.printStackTrace();
            }


        }
    }


    //............................................................................................

    private class GetVehicleModel extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(getActivity());


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                jsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "models/", "GET", json2);
                System.out.println("Model response in vehicle---   " + jsonArray.length());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                modelListResponseList.clear();
                for (int i = 0; i < jsonArray.length(); i++) {
                    if (brandListResponseList.get(spinnerViewList.get(0).getSelectedItemPosition()).toString().trim().equals(jsonArray.getJSONObject(i).getJSONObject("brand").getString("name").trim()) && categoryListResponseList.get(spinnerViewList.get(2).getSelectedItemPosition()).toString().trim().equals(jsonArray.getJSONObject(i).getJSONObject("category").getString("name").trim())) {
                        modelListResponseList.add(new CategoryListResponse(Integer.parseInt(jsonArray.getJSONObject(i).getString("id")), jsonArray.getJSONObject(i).getString("name"), ""));


                    }

                }

                modelListResponseArrayAdapter.notifyDataSetChanged();
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }


    //..............................................................................................

    private class AddVehicles extends AsyncTask<String, String, String> {
        String message = "", id = "";
        boolean flag;
        ProgressDialog progressBar;
        JSONArray _fetchUserProfileJsonArray;
        JSONObject _updateUserProfileOutputJSON, _updateUserProfileInput;
        ProgressDialog progressDialog = new ProgressDialog(getActivity());


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {


                id = _sharedPref.getStringData(Global_Data.id);
                _updateUserProfileInput = new JSONObject();

                _updateUserProfileInput.put("brand", brandListResponseList.get(spinnerViewList.get(0).getSelectedItemPosition()).getId());
                _updateUserProfileInput.put("category", categoryListResponseList.get(spinnerViewList.get(2).getSelectedItemPosition()).getId());
                _updateUserProfileInput.put("model", modelListResponseList.get(spinnerViewList.get(1).getSelectedItemPosition()).getId());
                _updateUserProfileInput.put("vehicle_number", carNumberEditText.getText().toString());
                _updateUserProfileInput.put("owner", _sharedPref.getStringData(Global_Data.id));
                _updateUserProfileInput.put("driver", JSONObject.NULL);


                if (frontsideviewnp_imageview_bitmap != null) {
                    _updateUserProfileInput.put("vehicle_front_image", encodeToBase64(frontsideviewnp_imageview_bitmap));
                }
                if (backsideviewnp_imageview_bitmap != null) {
                    _updateUserProfileInput.put("vehicle_back_image", encodeToBase64(backsideviewnp_imageview_bitmap));
                }
                if (leftsideview_imageview_bitmap != null) {
                    _updateUserProfileInput.put("vehicle_left_image", encodeToBase64(leftsideview_imageview_bitmap));
                }
                if (rightsideview_imageview_bitmap != null) {
                    _updateUserProfileInput.put("vehicle_right_image", encodeToBase64(rightsideview_imageview_bitmap));
                }
                if (Vehicle_tax_doc_frontview_imageview_bitmap != null) {
                    _updateUserProfileInput.put("vehicle_tax_doc_front_image", encodeToBase64(Vehicle_tax_doc_frontview_imageview_bitmap));
                }
                if (vehicle_tax_doc_back_imageview_bitmap != null) {
                    _updateUserProfileInput.put("vehicle_tax_doc_back_image", encodeToBase64(vehicle_tax_doc_back_imageview_bitmap));
                }
                if (vehicle_rc_card_front_imageview_bitmap != null) {
                    _updateUserProfileInput.put("vehicle_rc_card_front_image", encodeToBase64(vehicle_rc_card_front_imageview_bitmap));
                }
                if (vehicle_rc_card_back_imageview_bitmap != null) {
                    _updateUserProfileInput.put("vehicle_rc_card_back_image", encodeToBase64(vehicle_rc_card_back_imageview_bitmap));
                }
                String json2 = _updateUserProfileInput.toString();
                System.out.println("Driver id---   " + _sharedPref.getStringData(Global_Data.driver_id));
                //  json2 = _sharedPref.getStringData(Global_Data.driver_id);
                // json2 = _sharedPref.getStringData(Global_Data.id);
                JSONParser jsonParser = new JSONParser();
                System.out.println("_addVehiclesInput  ...   " + json2);
                _updateUserProfileOutputJSON = jsonParser.makeHttpRequest(Global_Data._url + "vehicles/", "POST", json2);
                System.out.println("_addVehiclesOUTPUT   ..    "+_updateUserProfileOutputJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (_updateUserProfileOutputJSON != null) {
                    Toast.makeText(getActivity(), "Updated vehicle", Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }


    public static String encodeToBase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        Log.e("LOOK", imageEncoded);
        return imageEncoded;
    }

}
