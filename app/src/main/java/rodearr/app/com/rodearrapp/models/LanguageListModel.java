package rodearr.app.com.rodearrapp.models;

/**
 * Created by wel come on 11-06-2018.
 */

public class LanguageListModel {

   private String name;

    public LanguageListModel(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
