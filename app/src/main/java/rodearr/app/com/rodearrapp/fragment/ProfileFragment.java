package rodearr.app.com.rodearrapp.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import rodearr.app.com.rodearrapp.R;
import rodearr.app.com.rodearrapp.global_supporting_items.Global_Data;
import rodearr.app.com.rodearrapp.jsonparser.JSONParser;
import rodearr.app.com.rodearrapp.models.AreaListResponse;
import rodearr.app.com.rodearrapp.models.CityListResponse;
import rodearr.app.com.rodearrapp.models.CountryListResponse;
import rodearr.app.com.rodearrapp.models.StateListResponse;
import rodearr.app.com.rodearrapp.sharedpreference.SharedPref;


public class ProfileFragment extends Fragment {
    @BindViews({R.id.gender_profile, R.id.area_profile, R.id.city_profile, R.id.state_profile, R.id.country_profile})
    List<Spinner> spinnerViewList;

    @BindViews({R.id.fname_profile, R.id.lname_profile, R.id.email_id_profile, R.id.mobile_number_profile, R.id.dob_profile, R.id.license_number_profile,
            R.id.address_profile, R.id.landmark_profile, R.id.house_number_profile, R.id.paytm_number_profile})
    List<EditText> editTextViewList;

    @BindView(R.id.save_profile)
    Button save_profile;

    @BindView(R.id.upload_driving_license)
    Button upload_driving_license;
    View view;
    @BindView(R.id.image_uploading_layout)
    LinearLayout image_uploading_layout;
    @BindView(R.id.profile_view)
    LinearLayout profile_view;

    private List<String> genderList;
    private ArrayAdapter<String> genderSpinnerArrayAdapter;
    private SharedPref _sharedPref;

    private ArrayAdapter<AreaListResponse> areaSpinnerArrayAdapter;
    private ArrayAdapter<CityListResponse> citySpinnerArrayAdapter;
    private ArrayAdapter<StateListResponse> stateSpinnerArrayAdapter;
    private ArrayAdapter<CountryListResponse> countrySpinnerArrayAdapter;

    List<AreaListResponse> areaListResponseList;
    List<StateListResponse> stateListResponseList;
    List<CountryListResponse> countryListResponseList;
    List<CityListResponse> cityListResponseList;
    ArrayList<AreaListResponse> areaListResponseArrayList;
    private int PICK_IMAGE_REQUEST = 100;

    public static final String TAG = "Upload Image";
    private Bitmap bitmap = null;
    @BindView(R.id.uploaded_imageview)
    ImageView imgView;
    private Uri filePath;

    private String selectedFilePath, _userAddressId;
    private Dialog calendarDialog;
    private CalendarView date_calendar;
    private DatePicker datePicker;
    ProgressDialog progressDialog;

    @BindView(R.id.paytm_number_text)
    TextView paytm_number_text;
    @BindView(R.id.email_text)
    TextView email_text;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_profile, container, false);
        ButterKnife.bind(this, view);
        _sharedPref = new SharedPref(getActivity());
        genderList = new ArrayList<>();
        genderList.add("Gender");
        genderList.add("Male");
        genderList.add("Female");
        progressDialog = new ProgressDialog(getActivity());

        areaListResponseArrayList = new ArrayList<>();
        stateListResponseList = new ArrayList<>();
        countryListResponseList = new ArrayList<>();
        cityListResponseList = new ArrayList<>();
        areaListResponseList = new ArrayList<>();
        profile_view.setVisibility(View.GONE);
        genderSpinnerArrayAdapter = new ArrayAdapter<String>
                (getActivity(), android.R.layout.simple_spinner_item,
                        genderList); //selected item will look like a spinner set from XML
        genderSpinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        spinnerViewList.get(0).setAdapter(genderSpinnerArrayAdapter);

        areaSpinnerArrayAdapter = new ArrayAdapter<AreaListResponse>
                (getActivity(), android.R.layout.simple_spinner_item,
                        areaListResponseArrayList);
       /* areaSpinnerArrayAdapter = new ArrayAdapter<String>
                (getActivity(), android.R.layout.simple_spinner_item,
                        are);*/ //selected item will look like a spinner set from XML
        areaSpinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        spinnerViewList.get(1).setAdapter(areaSpinnerArrayAdapter);

        citySpinnerArrayAdapter = new ArrayAdapter<CityListResponse>
                (getActivity(), android.R.layout.simple_spinner_item,
                        cityListResponseList); //selected item will look like a spinner set from XML
        citySpinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        spinnerViewList.get(2).setAdapter(citySpinnerArrayAdapter);
        stateSpinnerArrayAdapter = new ArrayAdapter<StateListResponse>
                (getActivity(), android.R.layout.simple_spinner_item,
                        stateListResponseList); //selected item will look like a spinner set from XML
        stateSpinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        spinnerViewList.get(3).setAdapter(stateSpinnerArrayAdapter);

        countrySpinnerArrayAdapter = new ArrayAdapter<CountryListResponse>
                (getActivity(), android.R.layout.simple_spinner_item,
                        countryListResponseList); //selected item will look like a spinner set from XML
        countrySpinnerArrayAdapter.setDropDownViewResource(android.R.layout
                .simple_spinner_dropdown_item);
        spinnerViewList.get(4).setAdapter(countrySpinnerArrayAdapter);

        calendarDialog = new Dialog(getActivity());
        calendarDialog.setContentView(R.layout.open_calendar);
        datePicker = (DatePicker) calendarDialog.findViewById(R.id.calendar);
        datePicker.setMaxDate(System.currentTimeMillis());
        datePicker.setMaxDate(System.currentTimeMillis());
        if (Global_Data.checkInternetConnection(getActivity())) {

            if (Build.VERSION.SDK_INT > 11) {

                new GetCountryListTask().executeOnExecutor(Global_Data.sExecutor);
            } else {

                new GetCountryListTask().execute();
            }
        } else {

            Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
        }

        // Inflate the layout for this fragment
        if (_sharedPref.getStringData(Global_Data.user_role).equals("Vehicle_Owner")) {
            paytm_number_text.setVisibility(View.VISIBLE);
            editTextViewList.get(9).setVisibility(View.VISIBLE);
        } else {
            editTextViewList.get(2).setVisibility(View.GONE);
            email_text.setVisibility(View.GONE);
            paytm_number_text.setVisibility(View.GONE);
            editTextViewList.get(9).setVisibility(View.GONE);
        }
        save_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

/*
                @BindViews({R.id.fname_profile, R.id.lname_profile, R.id.email_id_profile, R.id.mobile_number_profile, R.id.dob_profile, R.id.license_number_profile,
                        R.id.address_profile, R.id.landmark_profile, R.id.house_number_profile, R.id.paytm_number_profile})

*/

                if (editTextViewList.get(0).getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Please enter first name",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextViewList.get(1).getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Please enter last Name",
                            Toast.LENGTH_SHORT).show();
                } else if (_sharedPref.getStringData(Global_Data.user_role).equals("Vehicle_Owner") && editTextViewList.get(2).getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Please enter email",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextViewList.get(3).getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Please enter mobile number",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextViewList.get(3).getText().toString().length() > 10 || editTextViewList.get(3).getText().toString().length() < 10) {
                    Toast.makeText(getActivity(), "Please enter 10 digit mobile number",
                            Toast.LENGTH_SHORT).show();
                } else if (_sharedPref.getStringData(Global_Data.user_role).equals("Vehicle_Owner") && editTextViewList.get(9).getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Please enter mobile number",
                            Toast.LENGTH_SHORT).show();
                } else if (_sharedPref.getStringData(Global_Data.user_role).equals("Vehicle_Owner") && (editTextViewList.get(9).getText().toString().length() > 10 || editTextViewList.get(9).getText().toString().length() < 10)) {
                    Toast.makeText(getActivity(), "Please enter 10 digit mobile number",
                            Toast.LENGTH_SHORT).show();
                } else if (spinnerViewList.get(0).getSelectedItemPosition() == 0) {
                    Toast.makeText(getActivity(), "Please select gender",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextViewList.get(4).getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Please enter date of birth",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextViewList.get(5).getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Please enter license number",
                            Toast.LENGTH_SHORT).show();
                } else if (editTextViewList.get(6).getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Please enter address",
                            Toast.LENGTH_SHORT).show();
                }  else if (editTextViewList.get(8).getText().toString().length() == 0) {
                    Toast.makeText(getActivity(), "Please enter house number",
                            Toast.LENGTH_SHORT).show();
                } else {

                    if (Global_Data.checkInternetConnection(getActivity())) {

                        if (Build.VERSION.SDK_INT > 11) {
                            new UpdateUserProfile().executeOnExecutor(Global_Data.sExecutor);
                        } else {
                            new UpdateUserProfile().execute();
                        }
                    }
                }

                /*else if (editTextViewList.get(9).getText().toString().length() == 0) {

                } else if (editTextViewList.get(10).getText().toString().length() == 0) {

                } else if (editTextViewList.get(11).getVisibility() == View.VISIBLE && editTextViewList.get(11).getText().toString().length() == 0) {

                } else if (editTextViewList.get(11).getVisibility() == View.VISIBLE && (editTextViewList.get(11).getText().toString().length() > 10 || editTextViewList.get(11).getText().toString().length() < 10)) {
                    Toast.makeText(getActivity(), "Please enter 10 digit mobile number",
                            Toast.LENGTH_SHORT).show();
                }*/


            }
        });
        image_uploading_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // bitmap = null;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
            }
        });
        upload_driving_license.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  bitmap = null;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);
            }
        });
        editTextViewList.get(4).setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {


                calendarDialog.setTitle("Select Date");
                calendarDialog.show();
                getActivity().showDialog(999);
                return false;


            }


        });
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            datePicker.setOnDateChangedListener(new DatePicker.OnDateChangedListener() {
                @Override
                public void onDateChanged(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                   /* Toast.makeText(getActivity(),
                            "onDateChanged if  Year: " + year + "\n" +
                                    "Month of Year: " + monthOfYear + "\n" +
                                    "Day of Month: " + dayOfMonth , Toast.LENGTH_SHORT).show();
*/
                    try {
                        //Toast.makeText(getApplicationContext(), "check chala  ", Toast.LENGTH_SHORT).show();
                        int actual_month = monthOfYear + 1;
                        /*_userDob = dayOfMonth + "." + actual_month + "." + year;
                         */
                        String months = "", day = "";
                        if (actual_month < 10) {
                            months = "0" + actual_month;
                        } else {
                            months = "" + actual_month;
                        }
                        if (dayOfMonth < 10) {
                            day = "0" + dayOfMonth;
                        } else {
                            day = "" + dayOfMonth;
                        }

                        editTextViewList.get(4).setText(year + "-" + months + "-" + day);

                        calendarDialog.hide();


                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                   /* String date = "You picked the following date: "+dayOfMonth+"/"+(monthOfYear+1)+"/"+year;
                    editTextViewList.get(4).setText(date);*/
                }
            });
        } else {

            Calendar today = Calendar.getInstance();
            datePicker.init(today.get(Calendar.YEAR),
                    today.get(Calendar.MONTH),
                    today.get(Calendar.DAY_OF_MONTH),
                    new DatePicker.OnDateChangedListener() {

                        @Override
                        public void onDateChanged(DatePicker view,
                                                  int year, int monthOfYear, int dayOfMonth) {
                           /* Toast.makeText(getActivity(),
                                    "onDateChanged else Year: " + year + "\n" +
                                            "Month of Year: " + monthOfYear + "\n" +
                                            "Day of Month: " + dayOfMonth , Toast.LENGTH_SHORT).show();
                    */
                            try {
                                //Toast.makeText(getApplicationContext(), "check chala  ", Toast.LENGTH_SHORT).show();
                                int actual_month = monthOfYear + 1;
                                /*_userDob = dayOfMonth + "." + actual_month + "." + year;
                                 */
                                String months = "", day = "";
                                if (actual_month < 10) {
                                    months = "0" + actual_month;
                                } else {
                                    months = "" + actual_month;
                                }

                                if (dayOfMonth < 10) {
                                    day = "0" + dayOfMonth;
                                } else {
                                    day = "" + dayOfMonth;
                                }

                                editTextViewList.get(4).setText(year + "-" + months + "-" + day);

                                calendarDialog.hide();


                            } catch (Exception e) {
                                e.printStackTrace();
                            }


                        }
                    });
        }


        spinnerViewList.get(3).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (Global_Data.checkInternetConnection(getActivity())) {

                    if (Build.VERSION.SDK_INT > 11) {

                        new GetCityListTask().executeOnExecutor(Global_Data.sExecutor);
                    } else {

                        new GetCityListTask().execute();
                    }
                } else {

                    Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerViewList.get(4).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (Global_Data.checkInternetConnection(getActivity())) {

                    if (Build.VERSION.SDK_INT > 11) {

                        new GetStateListTask().executeOnExecutor(Global_Data.sExecutor);
                    } else {

                        new GetStateListTask().execute();
                    }
                } else {

                    Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinnerViewList.get(2).setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (Global_Data.checkInternetConnection(getActivity())) {

                    if (Build.VERSION.SDK_INT > 11) {

                        new GetAreaListTask().executeOnExecutor(Global_Data.sExecutor);
                    } else {

                        new GetAreaListTask().execute();
                    }
                } else {

                    Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Global_Data.checkInternetConnection(getActivity())) {

                    if (Build.VERSION.SDK_INT > 11) {
                        new FetchUserProfile().executeOnExecutor(Global_Data.sExecutor);
                    } else {
                        new FetchUserProfile().execute();
                    }
                }
            }
        }, 2000);

/*
        date_calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(CalendarView view, int year, int month, int dayOfMonth) {
                try {
                    //Toast.makeText(getApplicationContext(), "check chala  ", Toast.LENGTH_SHORT).show();
                    int actual_month = month + 1;
                    *//*_userDob = dayOfMonth + "." + actual_month + "." + year;
         *//*
                    String months = "";
                    if (actual_month < 10) {
                        months = "0" + actual_month;
                    } else {
                        months = "" + actual_month;
                    }


                    editTextViewList.get(4).setText(year + "-" + months + "-" + dayOfMonth);

                    calendarDialog.hide();


                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });*/

        return view;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        System.out.println("onActivityResult   " + requestCode + "        " + resultCode);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {
            System.out.println("requestCode");
            filePath = data.getData();
            selectedFilePath = getPath(filePath);
            Log.i(TAG, " File path : " + filePath);
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);
                persistImage(bitmap, "imagefile");
                System.out.println("Bitmap here--   " + bitmap.toString());
                imgView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private class FetchUserProfile extends AsyncTask<String, String, String> {
        String message = "";
        boolean flag;
        ProgressDialog progressBar;
        JSONArray _fetchUserProfileJsonArray;
        JSONObject _fetchUserProfileJSON;
        ProgressDialog progressDialog = new ProgressDialog(getActivity());

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {

                String json2 = "";
                //System.out.println("Driver id---   " + _sharedPref.getStringData(Global_Data.driver_id));
                //  json2 = _sharedPref.getStringData(Global_Data.driver_id);
                json2 = "";
                JSONParser jsonParser = new JSONParser();
                System.out.println("");
                if (_sharedPref.getStringData(Global_Data.user_role).equals("Vehicle_Driver")) {
                    System.out.println("Driver----    " + _sharedPref.getStringData(Global_Data.id));

                    _fetchUserProfileJSON = jsonParser.makeHttpRequest(Global_Data._url + "drivers/" + _sharedPref.getStringData(Global_Data.id), "GET", json2);

                } else {
                    System.out.println("Owner----    " + _sharedPref.getStringData(Global_Data.id));
                    _fetchUserProfileJSON = jsonParser.makeHttpRequest(Global_Data._url + "owners/" + _sharedPref.getStringData(Global_Data.id), "GET", json2);


                }   //   _fetchUserProfileJsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "drivers/"+_sharedPref.getStringData(Global_Data.id), "GET", json2);
                System.out.println("Profile output...   " + _fetchUserProfileJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (_fetchUserProfileJSON != JSONObject.NULL) {
                    System.out.println("Inside even it is null");
                    editTextViewList.get(0).setText(_fetchUserProfileJSON.getString("firstname"));
                    editTextViewList.get(1).setText(_fetchUserProfileJSON.getString("lastname"));
                    editTextViewList.get(3).setText(_fetchUserProfileJSON.getString("contact_number"));
                    editTextViewList.get(4).setText(_fetchUserProfileJSON.getString("dob"));
                    editTextViewList.get(5).setText(_fetchUserProfileJSON.getString("license_number"));
                    editTextViewList.get(6).setText(_fetchUserProfileJSON.getJSONObject("address").getString("address1"));
                    editTextViewList.get(7).setText(_fetchUserProfileJSON.getJSONObject("address").getString("nearby"));
                    editTextViewList.get(8).setText(_fetchUserProfileJSON.getJSONObject("address").getString("house"));

                    _userAddressId = _fetchUserProfileJSON.getJSONObject("address").getString("id");
                    if (_fetchUserProfileJSON.getString("gender").equals("Male")) {
                        spinnerViewList.get(0).setSelection(1);
                    } else {
                        spinnerViewList.get(0).setSelection(2);
                    }

                    final Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                spinnerViewList.get(2).setSelection(_fetchUserProfileJSON.getJSONObject("address").getInt("city") - 1);
                                spinnerViewList.get(3).setSelection(_fetchUserProfileJSON.getJSONObject("address").getInt("state") - 1);
                                spinnerViewList.get(4).setSelection(_fetchUserProfileJSON.getJSONObject("address").getInt("country") - 1);
                                spinnerViewList.get(1).setSelection(_fetchUserProfileJSON.getJSONObject("address").getInt("area") - 1);
                                System.out.println("coming here or not");
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }, 4000);
                   /*  spinnerViewList.get(2).setSelection(Integer.parseInt(_fetchUserProfileJSON.getJSONObject("address").getString("city")) - 1);
                    spinnerViewList.get(3).setSelection(Integer.parseInt(_fetchUserProfileJSON.getJSONObject("address").getString("state")) - 1);
                    spinnerViewList.get(4).setSelection(Integer.parseInt(_fetchUserProfileJSON.getJSONObject("address").getString("country")) - 1);
                    spinnerViewList.get(1).setSelection(Integer.parseInt(_fetchUserProfileJSON.getJSONObject("address").getString("area")) - 1);
              */    //  spinnerViewList.get(1).setSelection(areaListResponseArrayList.get());

                    //    spinnerViewList.get(1).setSelection();
                    if (_fetchUserProfileJSON.has("email") && _fetchUserProfileJSON.getString("email") != null) {
                        editTextViewList.get(2).setText(_fetchUserProfileJSON.getString("email"));

                    }
                    if (_fetchUserProfileJSON.has("paytm_number")) {

                        editTextViewList.get(9).setText(_fetchUserProfileJSON.getString("paytm_number"));

                    }

                    if (_fetchUserProfileJSON.has("dl_img_front")) {
                        if (!_fetchUserProfileJSON.isNull("dl_img_front")) {
                            Glide.with(getActivity()).load(_fetchUserProfileJSON.getString("dl_img_front"))
                                    //  Glide.with(getActivity()).load("https://s3.ap-south-1.amazonaws.com/rdrproject-media-bucket/android/default/backsideviewnp.png")
                                    .thumbnail(0.5f)
                                    .crossFade()
                                    //  .placeholder(R.drawable.driving_license)
                                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                                    .into(imgView);
                        }
                    }
                    profile_view.setVisibility(View.VISIBLE);
                } else {
                    Toast.makeText(getActivity(), "Details not found", Toast.LENGTH_SHORT).show();
                }
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }


    //............................................................................................


    private class UpdateUserProfile extends AsyncTask<String, String, String> {
        String message = "", id = "";
        boolean flag;
        ProgressDialog progressBar;
        JSONArray _fetchUserProfileJsonArray;
        JSONObject _updateUserProfileOutputJSON, _updateUserProfileInput, _updateUserAddressInput;
        ProgressDialog progressDialog = new ProgressDialog(getActivity());


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {


                id = _sharedPref.getStringData(Global_Data.id);
                _updateUserProfileInput = new JSONObject();
                _updateUserAddressInput = new JSONObject();
                _updateUserAddressInput.put("house", editTextViewList.get(8).getText().toString());
                _updateUserAddressInput.put("address1", editTextViewList.get(6).getText().toString());
                _updateUserAddressInput.put("nearby", editTextViewList.get(7).getText().toString());
                _updateUserAddressInput.put("area", spinnerViewList.get(1).getSelectedItemPosition() + 1);
                _updateUserAddressInput.put("city", spinnerViewList.get(2).getSelectedItemPosition() + 1);
                _updateUserAddressInput.put("state", spinnerViewList.get(3).getSelectedItemPosition() + 1);
                _updateUserAddressInput.put("country", spinnerViewList.get(4).getSelectedItemPosition() + 1);


                _updateUserProfileInput.put("id", id);

                if (_sharedPref.getStringData(Global_Data.user_role).equals("Vehicle_Owner")) {
                    _updateUserProfileInput.put("email", editTextViewList.get(2).getText().toString());
                    _updateUserProfileInput.put("paytm_number", editTextViewList.get(9).getText().toString());

                }
                _updateUserProfileInput.put("firstname", editTextViewList.get(0).getText().toString());
                _updateUserProfileInput.put("lastname", editTextViewList.get(1).getText().toString());
                _updateUserProfileInput.put("gender", genderList.get(spinnerViewList.get(0).getSelectedItemPosition()));
                _updateUserProfileInput.put("contact_number", editTextViewList.get(3).getText().toString());
                _updateUserProfileInput.put("dob", editTextViewList.get(4).getText().toString());

                _updateUserProfileInput.put("license_number", editTextViewList.get(5).getText().toString());
                //   _updateUserProfileInput.put("address", _updateUserAddressInput);
                if (bitmap != null) {
                    _updateUserProfileInput.put("dl_img_front", encodeToBase64(bitmap));
                }
                String json2 = _updateUserProfileInput.toString();
                System.out.println("Driver id---   " + _sharedPref.getStringData(Global_Data.driver_id));
                //  json2 = _sharedPref.getStringData(Global_Data.driver_id);
                // json2 = _sharedPref.getStringData(Global_Data.id);
                JSONParser jsonParser = new JSONParser();
                System.out.println("_updateUserProfileInput  ...   " + json2);
                if (_sharedPref.getStringData(Global_Data.user_role).equals("Vehicle_Driver")) {

                    _updateUserProfileOutputJSON = jsonParser.makeHttpRequest(Global_Data._url + "drivers/" + id + "/", "PUT", json2);

                } else {
                    _updateUserProfileOutputJSON = jsonParser.makeHttpRequest(Global_Data._url + "owners/" + id + "/", "PUT", json2);

                }
                System.out.println("Profile data----     " + _updateUserProfileOutputJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {
                if (_updateUserProfileOutputJSON != null) {
                    if (Global_Data.checkInternetConnection(getActivity())) {

                        if (Build.VERSION.SDK_INT > 11) {
                            new UpdateUserProfileAddress().executeOnExecutor(Global_Data.sExecutor);
                        } else {
                            new UpdateUserProfileAddress().execute();
                        }
                    }
                }
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }
    //.......................................................................................


    private class FetchUserAddressProfile extends AsyncTask<String, String, String> {
        String message = "", id = "";
        boolean flag;
        ProgressDialog progressBar;
        JSONArray _fetchUserProfileJsonArray;
        JSONObject _updateUserAddressProfileOutputJSON, _updateUserAddressProfileInput;
        ProgressDialog progressDialog = new ProgressDialog(getActivity());


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {


                id = _sharedPref.getStringData(Global_Data.id);
                _updateUserAddressProfileInput = new JSONObject();


                String json2 = "";
                System.out.println("Driver id---   " + _sharedPref.getStringData(Global_Data.driver_id));
                //  json2 = _sharedPref.getStringData(Global_Data.driver_id);
                // json2 = _sharedPref.getStringData(Global_Data.id);
                JSONParser jsonParser = new JSONParser();
                System.out.println("_updateUserProfileInput  ...   " + json2);
                _updateUserAddressProfileOutputJSON = jsonParser.makeHttpRequest(Global_Data._url + "address/" + id + "/", "PUT", json2);
                System.out.println(_fetchUserProfileJsonArray);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {


                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    //........................................................................................

    private String persistImage(Bitmap bitmap, String name) {
        File filesDir = getActivity().getFilesDir();
        File imageFile = new File(filesDir, name + ".jpg");
        String file_String = "";
        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            System.out.println("OS---   " + imageFile.getName());
            file_String = imageFile.toString();
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }
        return file_String;
    }


    public static String encodeToBase64(Bitmap image) {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);

        Log.e("LOOK", imageEncoded);
        return imageEncoded;
    }

    private class UpdateUserProfileAddress extends AsyncTask<String, String, String> {
        String message = "", id = "";
        boolean flag;
        ProgressDialog progressBar;
        JSONArray _fetchUserProfileJsonArray;
        JSONObject _updateUserProfileAddressOutputJSON, _updateUserProfileInput;
        ProgressDialog progressDialog = new ProgressDialog(getActivity());


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Please wait....");
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                //  @BindViews({R.id.fname_profile,R.id.lname_profile,R.id.email_id_profile,
                // R.id.mobile_number_profile,R.id.dob_profile,R.id.license_number_profile})
                if (_sharedPref.getStringData(Global_Data.user_role).equals("Vehicle_Owner")) {

                    id = _sharedPref.getStringData(Global_Data.id);
                } else {

                    id = _sharedPref.getStringData(Global_Data.id);
                }
                _updateUserProfileInput = new JSONObject();
/*
* @BindViews({R.id.gender_profile,R.id.area_profile,R.id.city_profile,R.id.state_profile,R.id.country_profile})
    List<Spinner> spinnerViewList;

* */
                _updateUserProfileInput.put("id", Integer.parseInt(_userAddressId));
                _updateUserProfileInput.put("house", editTextViewList.get(8).getText().toString());
                _updateUserProfileInput.put("address1", editTextViewList.get(6).getText().toString());
                _updateUserProfileInput.put("nearby", editTextViewList.get(7).getText().toString());
                _updateUserProfileInput.put("area", spinnerViewList.get(1).getSelectedItemPosition() + 1);
                _updateUserProfileInput.put("city", spinnerViewList.get(2).getSelectedItemPosition() + 1);
                _updateUserProfileInput.put("state", spinnerViewList.get(3).getSelectedItemPosition() + 1);
                _updateUserProfileInput.put("country", spinnerViewList.get(4).getSelectedItemPosition() + 1);


                String json2 = _updateUserProfileInput.toString();
                System.out.println("Driver id---   " + id);
                //  json2 = _sharedPref.getStringData(Global_Data.driver_id);

                JSONParser jsonParser = new JSONParser();
                System.out.println("_updateUserProfileInput----   " + _updateUserProfileInput);
                _updateUserProfileAddressOutputJSON = jsonParser.makeHttpRequest(Global_Data._url + "address/" + _userAddressId + "/", "PUT", json2);
                System.out.println(_updateUserProfileAddressOutputJSON);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            try {

                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }


    //.................................................

    private class GetAreaListTask extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                jsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "area/", "GET", json2);
                System.out.println("Brand response---   " + jsonArray.length());
            } catch (Exception e) {
                e.printStackTrace();

            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                areaListResponseArrayList.clear();
               /*// areaListResponses = null;
                areaListResponses = new AreaListResponse[jsonArray.length()];*/
                for (int i = 0; i < jsonArray.length(); i++) {
                   /* areaListResponseList.add(new AreaListResponse(jsonArray.getJSONObject(i).getString("url"), jsonArray.getJSONObject(i).getInt("id"),
                            jsonArray.getJSONObject(i).getString("name"), jsonArray.getJSONObject(i).getString("code"), jsonArray.getJSONObject(i).getString("pincode"), jsonArray.getJSONObject(i).getInt("city"), jsonArray.getJSONObject(i).getInt("state")));
*/
                    if (String.valueOf(jsonArray.getJSONObject(i).getInt("city")).trim().equals(cityListResponseList.get(spinnerViewList.get(2).getSelectedItemPosition()).getId().toString().trim())) {

                        areaListResponseArrayList.add(new AreaListResponse("", jsonArray.getJSONObject(i).getInt("id"),
                                jsonArray.getJSONObject(i).getString("name"), jsonArray.getJSONObject(i).getString("code"), jsonArray.getJSONObject(i).getString("pincode"), jsonArray.getJSONObject(i).getInt("city"), jsonArray.getJSONObject(i).getInt("state")));
                   /* areaListResponses[i] = new AreaListResponse();
                    areaListResponses[i].setCity(jsonArray.getJSONObject(i).getInt("city"));
                    areaListResponses[i].setCode(jsonArray.getJSONObject(i).getString("code"));
                    areaListResponses[i].setId(jsonArray.getJSONObject(i).getInt("id"));
                    areaListResponses[i].setName(jsonArray.getJSONObject(i).getString("name"));
                    areaListResponses[i].setPincode(jsonArray.getJSONObject(i).getString("pincode"));
                    areaListResponses[i].setState(jsonArray.getJSONObject(i).getInt("state"));
                    areaListResponses[i].setUrl(jsonArray.getJSONObject(i).getString("url"));*/

                    }
                }
                areaSpinnerArrayAdapter.notifyDataSetChanged();
                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
                if (Global_Data.checkInternetConnection(getActivity())) {

                    if (Build.VERSION.SDK_INT > 11) {
                        new GetAreaListTask().executeOnExecutor(Global_Data.sExecutor);

                    } else {
                        new GetAreaListTask().execute();

                    }
                } else {

                    Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
            }


        }
    }

  /*  private class GetAreaListTask extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(getActivity());


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Loading....");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                jsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "area/", "GET", json2);
                System.out.println("Brand response---   " + jsonArray.length());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {

                 for (int i = 0; i < jsonArray.length(); i++) {


                    areaListResponseArrayList.add(new AreaListResponse("", jsonArray.getJSONObject(i).getInt("id"),
                            jsonArray.getJSONObject(i).getString("name"), jsonArray.getJSONObject(i).getString("code"), jsonArray.getJSONObject(i).getString("pincode"), jsonArray.getJSONObject(i).getInt("city"), jsonArray.getJSONObject(i).getInt("state")));
                        areaSpinnerArrayAdapter.notifyDataSetChanged();
                }

                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }*/


  /*  private class GetCityListTask extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(getActivity());


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Loading....");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                jsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "city/", "GET", json2);
                System.out.println("Brand response---   " + jsonArray.length());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {

                 for (int i = 0; i < jsonArray.length(); i++) {


                    cityListResponseList.add(new CityListResponse(jsonArray.getJSONObject(i).getString("url"), jsonArray.getJSONObject(i).getInt("id"),
                            jsonArray.getJSONObject(i).getString("name"), jsonArray.getJSONObject(i).getString("code"), jsonArray.getJSONObject(i).getInt("state")));

                    citySpinnerArrayAdapter.notifyDataSetChanged();
                }

                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }*/

  /*  private class GetStateListTask extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(getActivity());


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Loading....");
            progressDialog.setCanceledOnTouchOutside(false);
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                jsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "state/", "GET", json2);
                System.out.println("Brand response---   " + jsonArray.length());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {

                for (int i = 0; i < jsonArray.length(); i++) {


                    stateListResponseList.add(new StateListResponse(jsonArray.getJSONObject(i).getString("url"), jsonArray.getJSONObject(i).getInt("id"),
                            jsonArray.getJSONObject(i).getString("name"), jsonArray.getJSONObject(i).getString("code"), jsonArray.getJSONObject(i).getInt("country")));

                    stateSpinnerArrayAdapter.notifyDataSetChanged();
                }

                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }*/

    /*private class GetCountryListTask extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;
        ProgressDialog progressDialog = new ProgressDialog(getActivity());


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Loading....");
            progressDialog.setCanceledOnTouchOutside(false);
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                jsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "countries/", "GET", json2);
                System.out.println("Brand response---   " + jsonArray.length());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {

                 for (int i = 0; i < jsonArray.length(); i++) {


                    countryListResponseList.add(new CountryListResponse(jsonArray.getJSONObject(i).getInt("id"),
                            jsonArray.getJSONObject(i).getString("name"), jsonArray.getJSONObject(i).getString("code")));
                           countrySpinnerArrayAdapter.notifyDataSetChanged();
                }
                    if (Global_Data.checkInternetConnection(getActivity())) {

                        if (Build.VERSION.SDK_INT > 11) {
                            new FetchUserProfile().executeOnExecutor(Global_Data.sExecutor);
                        } else {
                            new FetchUserProfile().execute();
                        }
                }

                progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }*/


    private class GetCountryListTask extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;
        /*ProgressDialog progressDialog = new ProgressDialog(getActivity());
         */

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog.setMessage("Loading....");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.setCancelable(false);
            progressDialog.show();
           /* progressDialog.setMessage("Loading....");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();*/
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                jsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "countries/", "GET", json2);
                System.out.println("Country response---   " + jsonArray.length() + "   ..   " + jsonArray.getJSONObject(0).getString("name"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                countryListResponseList.clear();
               /*// areaListResponses = null;
                areaListResponses = new AreaListResponse[jsonArray.length()];*/
                System.out.println("Outside filling");
                for (int i = 0; i < jsonArray.length(); i++) {
                   /* areaListResponseList.add(new AreaListResponse(jsonArray.getJSONObject(i).getString("url"), jsonArray.getJSONObject(i).getInt("id"),
                            jsonArray.getJSONObject(i).getString("name"), jsonArray.getJSONObject(i).getString("code"), jsonArray.getJSONObject(i).getString("pincode"), jsonArray.getJSONObject(i).getInt("city"), jsonArray.getJSONObject(i).getInt("state")));
*/
                    System.out.println("Inside filling");
                    countryListResponseList.add(new CountryListResponse(jsonArray.getJSONObject(i).getInt("id"),
                            jsonArray.getJSONObject(i).getString("name"), jsonArray.getJSONObject(i).getString("code")));
                   /* areaListResponses[i] = new AreaListResponse();
                    areaListResponses[i].setCity(jsonArray.getJSONObject(i).getInt("city"));
                    areaListResponses[i].setCode(jsonArray.getJSONObject(i).getString("code"));
                    areaListResponses[i].setId(jsonArray.getJSONObject(i).getInt("id"));
                    areaListResponses[i].setName(jsonArray.getJSONObject(i).getString("name"));
                    areaListResponses[i].setPincode(jsonArray.getJSONObject(i).getString("pincode"));
                    areaListResponses[i].setState(jsonArray.getJSONObject(i).getInt("state"));
                    areaListResponses[i].setUrl(jsonArray.getJSONObject(i).getString("url"));*/
                    countrySpinnerArrayAdapter.notifyDataSetChanged();
                }

                //  progressDialog.dismiss();
                if (Global_Data.checkInternetConnection(getActivity())) {

                    if (Build.VERSION.SDK_INT > 11) {

                        new GetStateListTask().executeOnExecutor(Global_Data.sExecutor);

                    } else {

                        new GetStateListTask().execute();

                    }
                } else {

                    Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }

            } catch (Exception e) {
                e.printStackTrace();
                if (Global_Data.checkInternetConnection(getActivity())) {

                    if (Build.VERSION.SDK_INT > 11) {

                        new GetCountryListTask().executeOnExecutor(Global_Data.sExecutor);
                    } else {

                        new GetCountryListTask().execute();
                    }
                } else {

                    Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
            }


        }
    }

    //...........................


    private class GetStateListTask extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;
        //  ProgressDialog progressDialog = new ProgressDialog(getActivity());


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
          /*  progressDialog.setMessage("Loading....");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();*/
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                jsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "state/", "GET", json2);
                System.out.println("State response---   " + jsonArray.length());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                stateListResponseList.clear();
               /*// areaListResponses = null;
                areaListResponses = new AreaListResponse[jsonArray.length()];*/
                for (int i = 0; i < jsonArray.length(); i++) {

                    //  System.out.println("---????   " + jsonArray.getJSONObject(i).getInt("country") + "   ///   " + countryListResponseList.get(spinnerViewList.get(3).getSelectedItemPosition()).getId());
                    if (String.valueOf(jsonArray.getJSONObject(i).getInt("country")).trim().equals(countryListResponseList.get(spinnerViewList.get(4).getSelectedItemPosition()).getId().toString().trim())) {

                        stateListResponseList.add(new StateListResponse(jsonArray.getJSONObject(i).getString("url"), jsonArray.getJSONObject(i).getInt("id"),
                                jsonArray.getJSONObject(i).getString("name"), jsonArray.getJSONObject(i).getString("code"), jsonArray.getJSONObject(i).getInt("country")));


                    }
                }
                stateSpinnerArrayAdapter.notifyDataSetChanged();
                if (Global_Data.checkInternetConnection(getActivity())) {

                    if (Build.VERSION.SDK_INT > 11) {

                        new GetCityListTask().executeOnExecutor(Global_Data.sExecutor);

                    } else {

                        new GetCityListTask().execute();

                    }
                } else {

                    Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
                //  progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();

                if (Global_Data.checkInternetConnection(getActivity())) {

                    if (Build.VERSION.SDK_INT > 11) {

                        new GetStateListTask().executeOnExecutor(Global_Data.sExecutor);

                    } else {

                        new GetStateListTask().execute();

                    }
                } else {

                    Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
            }


        }
    }

    //................................


    private class GetCityListTask extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;
        /*  ProgressDialog progressDialog = new ProgressDialog(getActivity());
         */

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
          /*  progressDialog.setMessage("Loading....");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();*/
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                String json2 = "";
                System.out.println(json2);
                JSONParser jsonParser = new JSONParser();
                jsonArray = jsonParser.makeHttpRequestForJSONArray(Global_Data._url + "city/", "GET", json2);
                System.out.println("city response---   " + jsonArray.length());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {
                cityListResponseList.clear();
                for (int i = 0; i < jsonArray.length(); i++) {


                    System.out.println("---++++ city---     " + jsonArray.getJSONObject(i).getInt("state") + "   ///     " + stateListResponseList.get(spinnerViewList.get(3).getSelectedItemPosition()).getId());
                    if (String.valueOf(jsonArray.getJSONObject(i).getInt("state")).trim().equals(stateListResponseList.get(spinnerViewList.get(3).getSelectedItemPosition()).getId().toString().trim())) {

                        cityListResponseList.add(new CityListResponse(jsonArray.getJSONObject(i).getString("url"), jsonArray.getJSONObject(i).getInt("id"),
                                jsonArray.getJSONObject(i).getString("name"), jsonArray.getJSONObject(i).getString("code"), jsonArray.getJSONObject(i).getInt("state")));

                    }
                }
                citySpinnerArrayAdapter.notifyDataSetChanged();
                if (Global_Data.checkInternetConnection(getActivity())) {

                    if (Build.VERSION.SDK_INT > 11) {
                        new GetAreaListTask().executeOnExecutor(Global_Data.sExecutor);

                    } else {
                        new GetAreaListTask().execute();

                    }
                } else {

                    Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
                // progressDialog.dismiss();

            } catch (Exception e) {
                e.printStackTrace();
                if (Global_Data.checkInternetConnection(getActivity())) {

                    if (Build.VERSION.SDK_INT > 11) {

                        new GetCityListTask().executeOnExecutor(Global_Data.sExecutor);

                    } else {

                        new GetCityListTask().execute();

                    }
                } else {

                    Toast.makeText(getActivity(), "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
            }


        }
    }
}
