package rodearr.app.com.rodearrapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import rodearr.app.com.rodearrapp.R;
import rodearr.app.com.rodearrapp.activity.DashboardActivity;
import rodearr.app.com.rodearrapp.global_supporting_items.Global_Data;
import rodearr.app.com.rodearrapp.models.VehicleALL;
import rodearr.app.com.rodearrapp.sharedpreference.SharedPref;

/**
 * Created by wel come on 07-06-2018.
 */

public class VehicleWithDriverListAdapter extends RecyclerView.Adapter<VehicleWithDriverListAdapter.MyViewHolder> {
    private ArrayList<Integer> IMAGES;
    Context mContext;
    private ArrayList<String> nameList;

    ArrayList<VehicleALL> vehicleListResponseList;

    SharedPref sharedPref;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView name;
        ImageView image;
        LinearLayout vehicle_number_layout;

        public MyViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.name);
            vehicle_number_layout = (LinearLayout) view.findViewById(R.id.vehicle_number_layout);
        }
    }


    public VehicleWithDriverListAdapter(ArrayList<VehicleALL> vehicleListResponseList
            , Context mContext) {
        sharedPref = new SharedPref(mContext);

        this.vehicleListResponseList = vehicleListResponseList;

        this.mContext = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.vehiclelistitem, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        holder.name.setText(vehicleListResponseList.get(position).getVehicleNumber());
        if (vehicleListResponseList.get(position).getDriver().getFirstname().trim().equals("")) {

            holder.name.setTextColor(Color.RED);
        } else {
            holder.name.setTextColor(mContext.getResources().getColor(R.color.color_green));
        }
        holder.vehicle_number_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(mContext, DashboardActivity.class);
                System.out.println("---????   " + vehicleListResponseList.get(position).getId());
                i.putExtra("VEHICLE", vehicleListResponseList);
                i.putExtra("POS", position);
                //  i.putExtra("number", vehicleListResponseList.get(position).getVehicleNumber());
                mContext.startActivity(i);
                sharedPref.setIntegerData(Global_Data.pos, position);
                sharedPref.setIntegerData(Global_Data.vid, vehicleListResponseList.get(position).getId());
            }
        });
        //  holder.image.setImageResource(IMAGES.get(position));
       /* Movie movie = moviesList.get(position);

        holder.genre.setText(movie.getGenre());
        holder.year.setText(movie.getYear());*/
    }

    @Override
    public int getItemCount() {
        return vehicleListResponseList.size();
    }
}
