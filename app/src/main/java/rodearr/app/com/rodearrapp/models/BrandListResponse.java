package rodearr.app.com.rodearrapp.models;

/**
 * Created by wel come on 10-06-2018.
 */
import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
public class BrandListResponse {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("vehicle_model")
    @Expose
    private List<VehicleModel> vehicleModel = null;

    public BrandListResponse(Integer id, String name, String code, List<VehicleModel> vehicleModel) {
        this.id = id;
        this.name = name;
        this.code = code;
        this.vehicleModel = vehicleModel;
    }



    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public List<VehicleModel> getVehicleModel() {
        return vehicleModel;
    }

    public void setVehicleModel(List<VehicleModel> vehicleModel) {
        this.vehicleModel = vehicleModel;
    }
    @Override
    public String toString() {
        return name;
    }

    public static class VehicleModel {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("category")
        @Expose
        private Category category;

        public VehicleModel(Integer id, String name, Category category) {
            this.id = id;
            this.name = name;
            this.category = category;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public Category getCategory() {
            return category;
        }

        public void setCategory(Category category) {
            this.category = category;
        }

    }

    public static class Category {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("desc")
        @Expose
        private String desc;

        public Category(Integer id, String name, String desc) {
            this.id = id;
            this.name = name;
            this.desc = desc;
        }

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDesc() {
            return desc;
        }

        public void setDesc(String desc) {
            this.desc = desc;
        }

    }
}
