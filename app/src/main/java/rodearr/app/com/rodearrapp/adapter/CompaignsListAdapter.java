package rodearr.app.com.rodearrapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.ArrayList;
import java.util.List;

import rodearr.app.com.rodearrapp.R;
import rodearr.app.com.rodearrapp.models.Compaign_response;

/**
 * Created by wel come on 07-06-2018.
 */

public class CompaignsListAdapter extends RecyclerView.Adapter<CompaignsListAdapter.MyViewHolder> {
    private List<Compaign_response> moviesList;
    private ArrayList<Integer> IMAGES;
    Context mContext;
    private ArrayList<String> url;
    private ArrayList<String> movie_name;
    private ArrayList<String> ratings;
    private ArrayList<String> votes;
    private ArrayList<String> type;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView you_drove_month_kmsvalue, you_drove_week_kmsvalue;
        ImageView compaign_image;

        public MyViewHolder(View view) {
            super(view);
            compaign_image = (ImageView) view.findViewById(R.id.compaign_image);
            you_drove_week_kmsvalue = (TextView) view.findViewById(R.id.you_drove_week_kmsvalue);
            you_drove_month_kmsvalue = (TextView) view.findViewById(R.id.you_drove_month_kmsvalue);

        }
    }


    public CompaignsListAdapter(List<Compaign_response> moviesList) {
        this.moviesList = moviesList;

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.compaign_list_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.you_drove_month_kmsvalue.setText(moviesList.get(position).getEarning());

        holder.you_drove_week_kmsvalue.setText(moviesList.get(position).getDuration());
        Glide.with(mContext).load(moviesList.get(position).getImage())
                .thumbnail(0.5f)
                .crossFade()

                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.compaign_image);
        //  holder.image.setImageResource(IMAGES.get(position));
       /* Movie movie = moviesList.get(position);

        holder.genre.setText(movie.getGenre());
        holder.year.setText(movie.getYear());*/
    }

    @Override
    public int getItemCount() {
        return moviesList.size();
    }
}
