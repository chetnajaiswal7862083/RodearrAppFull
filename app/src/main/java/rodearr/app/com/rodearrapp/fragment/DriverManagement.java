package rodearr.app.com.rodearrapp.fragment;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import rodearr.app.com.rodearrapp.R;
import rodearr.app.com.rodearrapp.sharedpreference.SharedPref;


public class DriverManagement extends Fragment {
    @BindViews({R.id.change_frontsideview_np_layout1, R.id.change_backsideview_np_layout, R.id.change_leftsideview_np_layout1, R.id.upload_leftsideview_layout1, R.id.upload_vehicle_tax_doc_frontview_layout1,
            R.id.upload_vehicle_tax_doc_back_layout1, R.id.upload_vehicle_rc_card_front_layout1, R.id.upload_vehicle_rc_card_back_layout1})
    List<LinearLayout> linearLayoutsViewList;

    @BindViews({R.id.change_frontsideview_np, R.id.change_backsideview_np, R.id.change_leftsideview_np, R.id.upload_leftsideview, R.id.upload_rightsideview, R.id.upload_vehicle_tax_doc_back,
            R.id.upload_vehicle_rc_card_front, R.id.upload_vehicle_rc_card_back})
    List<Button> btnViewList;
    private int PICK_IMAGE_REQUEST = 100;
    private int imageType = 9;

    View view,view1;
    public static final String TAG = "Upload Image";
    private Bitmap bitmap = null, frontsideviewnp_imageview_bitmap = null, backsideviewnp_imageview_bitmap = null, leftsideview_imageview_bitmap = null, rightsideview_imageview_bitmap = null, Vehicle_tax_doc_frontview_imageview_bitmap = null,
            vehicle_tax_doc_back_imageview_bitmap = null, vehicle_rc_card_front_imageview_bitmap = null, vehicle_rc_card_back_imageview_bitmap = null;
    @BindViews({R.id.frontsideviewnp_imageview, R.id.backsideviewnp_imageview1, R.id.leftsideview_imageview1, R.id.rightsideview_imageview1, R.id.Vehicle_tax_doc_frontview_imageview1, R.id.vehicle_tax_doc_back_imageview1, R.id.vehicle_rc_card_front_imageview1, R.id.vehicle_rc_card_back_imageview1})
    List<ImageView> imageViewList;

    @BindViews({R.id.add_vehicle_doc_layout,R.id.add_vehicle_detail_layout})
            List<ConstraintLayout> constraintLayoutViewList;

    ImageView imgView, frontsideviewnp_imageview, backsideviewnp_imageview, leftsideviewnp_imageview, leftsideview_imageview, rightsideview_imageview, rcbook_imageview, vehicle_taxpaper_imageview, vehicle_travel_permit_imageview;
    SharedPref _sharedPref;
    private Uri filePath;

    private String selectedFilePath;

    @BindView(R.id.save_vehicle_settings)
    Button save_vehicle_settings;

    @BindView(R.id.manage_driver)
    Button manage_driver;
    public DriverManagement() {
        // Required empty public constructor
    }



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_driver_management, container, false);
     //   view1 = inflater.inflate(R.layout.activity_home, container, false);
       /* TextView top=(TextView) view1.findViewById(R.id.top_title);
        top.setText("jhhjvhj");*/
        ButterKnife.bind(this, view);
        // Inflate the layout for this fragment
        linearLayoutsViewList.get(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                imageType = 0;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });

        linearLayoutsViewList.get(1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 1;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });

        linearLayoutsViewList.get(2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 2;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });

        linearLayoutsViewList.get(3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 3;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });

        linearLayoutsViewList.get(4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 4;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });

        linearLayoutsViewList.get(5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 5;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });

        linearLayoutsViewList.get(6).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 6;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });

        linearLayoutsViewList.get(7).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 7;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });
//....................................................................

        btnViewList.get(0).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 0;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });

        btnViewList.get(1).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 1;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });

        btnViewList.get(2).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 2;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });

        btnViewList.get(3).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 3;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });

        btnViewList.get(4).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 4;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });

        btnViewList.get(5).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 5;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });

        btnViewList.get(6).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 6;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });

        btnViewList.get(7).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageType = 7;
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Image"), PICK_IMAGE_REQUEST);

            }
        });

        save_vehicle_settings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                constraintLayoutViewList.get(1).setVisibility(View.VISIBLE);
                constraintLayoutViewList.get(0).setVisibility(View.GONE);

            }
        });
        manage_driver.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                constraintLayoutViewList.get(0).setVisibility(View.VISIBLE);
                constraintLayoutViewList.get(1).setVisibility(View.GONE);

            }
        });
        return view;
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == Activity.RESULT_OK && data != null && data.getData() != null) {

            filePath = data.getData();
            selectedFilePath = getPath(filePath);
            Log.i(TAG, " File path : " + filePath);
            try {
                bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), filePath);
                persistImage(bitmap, "imagefile");

   /*
   frontsideviewnp_imageview_bitmap = null, backsideviewnp_imageview_bitmap = null, leftsideview_imageview_bitmap = null, rightsideview_imageview_bitmap = null, Vehicle_tax_doc_frontview_imageview_bitmap = null,
            vehicle_tax_doc_back_imageview_bitmap = null, vehicle_rc_card_front_imageview_bitmap = null, vehicle_rc_card_back_imageview_bitmap = null;
*/

                if (imageType == 0) {
                    frontsideviewnp_imageview_bitmap = bitmap;
                    imageViewList.get(0).setImageBitmap(bitmap);
                } else if (imageType == 1) {
                    backsideviewnp_imageview_bitmap = bitmap;
                    imageViewList.get(1).setImageBitmap(bitmap);
                } else if (imageType == 2) {
                    leftsideview_imageview_bitmap = bitmap;
                    imageViewList.get(2).setImageBitmap(bitmap);
                } else if (imageType == 3) {
                    rightsideview_imageview_bitmap = bitmap;
                    imageViewList.get(3).setImageBitmap(bitmap);

                } else if (imageType == 4) {
                    Vehicle_tax_doc_frontview_imageview_bitmap = bitmap;
                    imageViewList.get(4).setImageBitmap(bitmap);
                } else if (imageType == 5) {
                    vehicle_tax_doc_back_imageview_bitmap = bitmap;
                    imageViewList.get(5).setImageBitmap(bitmap);
                } else if (imageType == 6) {
                    vehicle_rc_card_front_imageview_bitmap = bitmap;
                    imageViewList.get(6).setImageBitmap(bitmap);
                } else if (imageType == 7) {
                    vehicle_rc_card_back_imageview_bitmap = bitmap;
                    imageViewList.get(7).setImageBitmap(bitmap);
                }
                // imgView.setImageBitmap(bitmap);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    public String getPath(Uri uri) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    private String persistImage(Bitmap bitmap, String name) {
        File filesDir = getActivity().getFilesDir();
        File imageFile = new File(filesDir, name + ".jpg");
        String file_String = "";
        OutputStream os;
        try {
            os = new FileOutputStream(imageFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, os);
            System.out.println("OS---   " + imageFile.getName());
            file_String = imageFile.toString();
            os.flush();
            os.close();
        } catch (Exception e) {
            Log.e(getClass().getSimpleName(), "Error writing bitmap", e);
        }
        return file_String;
    }


}
