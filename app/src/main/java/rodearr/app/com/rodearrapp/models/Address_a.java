package rodearr.app.com.rodearrapp.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by wel come on 08-06-2018.
 */

public class Address_a {
    @SerializedName("user")
    private User user;

    @SerializedName("house")
    private String house;

    @SerializedName("address1")
    private String address1;

    @SerializedName("nearby")
    private String nearby;

    @SerializedName("area")
    private String area;

    @SerializedName("city")
    private String city;

    @SerializedName("url")
    private String url;
    @SerializedName("state")
    private String state;

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getNearby() {
        return nearby;
    }

    public void setNearby(String nearby) {
        this.nearby = nearby;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    private String country;
}
