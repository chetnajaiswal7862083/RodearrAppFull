package rodearr.app.com.rodearrapp.interfaces;

/*import retrofit2.Callback;
import retrofit2.http.GET;*/
import java.util.List;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.POST;
import rodearr.app.com.rodearrapp.models.RegisterModel;
import rodearr.app.com.rodearrapp.models.RegisterModel_a;


/**
 * Created by wel come on 08-06-2018.
 */

public interface APIInterface {
    @GET("/drivers")
    public void getObject(Callback<RegisterModel> student);

    @POST("/drivers")
    void registerUser(@Body String User, Callback<RegisterModel_a> cb);
/*    @POST("/drivers")
    void registerUser(@Body RegisterModel User, Callback<List<RegisterModel_a>> cb);*/
}
