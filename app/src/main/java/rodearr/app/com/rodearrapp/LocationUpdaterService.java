package rodearr.app.com.rodearrapp;

import android.Manifest;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import rodearr.app.com.rodearrapp.global_supporting_items.Global_Data;
import rodearr.app.com.rodearrapp.jsonparser.JSONParser;
import rodearr.app.com.rodearrapp.sharedpreference.SharedPref;


public class LocationUpdaterService extends Service {
    public static final int TWO_MINUTES = 60000; // 60 seconds
    public static Boolean isRunning = false;

    public LocationManager mLocationManager;
    public LocationUpdaterListener mLocationListener;
    public Location previousBestLocation = null;
    SharedPref _sharedPref;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        _sharedPref = new SharedPref(LocationUpdaterService.this);

        mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        mLocationListener = new LocationUpdaterListener();
        super.onCreate();
    }

    Handler mHandler = new Handler();
    Runnable mHandlerTask = new Runnable() {
        @Override
        public void run() {
            if (!isRunning) {
                startListening();
            }
            mHandler.postDelayed(mHandlerTask, TWO_MINUTES);
        }
    };

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        mHandlerTask.run();
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        // stopListening();
        mHandler.removeCallbacks(mHandlerTask);
        super.onDestroy();
    }

    private void startListening() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            if (mLocationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER))
                mLocationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, mLocationListener);

            if (mLocationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER))
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, mLocationListener);
        }
        isRunning = true;
    }

    private void stopListening() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            mLocationManager.removeUpdates(mLocationListener);
        }
        isRunning = false;
    }

    public class LocationUpdaterListener implements LocationListener {
        @Override
        public void onLocationChanged(Location location) {
            latitude = location.getLatitude();
            longitude = location.getLongitude();

            System.out.println("////////////   ....   "+ (!_sharedPref.getStringData(Global_Data.owner_driver_id).equals("")));
            if (_sharedPref.getStringData(Global_Data.user_role).equalsIgnoreCase("Vehicle_Driver") || (!_sharedPref.getStringData(Global_Data.owner_driver_id).equals(""))) {

                //   Toast.makeText(LocationUpdaterService.this, "Location in background..---   "+latitude, Toast.LENGTH_SHORT).show();

                if (Global_Data.checkInternetConnection(LocationUpdaterService.this)) {
                    if (Build.VERSION.SDK_INT > 11) {

                        new SendGeoLocation().executeOnExecutor(Global_Data.sExecutor);
                    } else {

                        new SendGeoLocation().execute();
                    }

                } else {

                    //  Toast.makeText(LocationUpdaterService.this, "Please check internet connection.", Toast.LENGTH_SHORT).show();
                }
            }
            if (isBetterLocation(location, previousBestLocation)) {
                previousBestLocation = location;
                try {


                    // Script to post location data to server..
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    stopListening();
                }
            }
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderDisabled(String provider) {
            stopListening();
        }

        @Override
        public void onProviderEnabled(String provider) {
        }


    }

    double latitude = 0.0, longitude = 0.0;

    private class SendGeoLocation extends AsyncTask<String, String, String> {

        ProgressDialog progressBar;
        JSONObject _brandListJSON;
        JSONArray jsonArray;
        JSONObject _updateUserProfileOutputJSON, _updateUserProfileInput;

        ProgressDialog progressDialog = new ProgressDialog(LocationUpdaterService.this);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
          /*  progressDialog.setMessage("Please wait....");
            progressDialog.show();*/
        }

        @Override
        protected String doInBackground(String... params) {
            try {
                Calendar c = Calendar.getInstance();
                System.out.println("Current time => " + c.getTime());

                SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
                String formattedDate = df.format(c.getTime());
                _updateUserProfileInput = new JSONObject();

                _updateUserProfileInput.put("reading_date", formattedDate.substring(0, 10));
                _updateUserProfileInput.put("reading_time", formattedDate.substring(11, 19));
                _updateUserProfileInput.put("lattitude", latitude);
                _updateUserProfileInput.put("longitude", longitude);
                if (_sharedPref.getStringData(Global_Data.user_role).equalsIgnoreCase("Vehicle_Driver")) {
                    _updateUserProfileInput.put("driver", _sharedPref.getStringData(Global_Data.id));
                } else if (!_sharedPref.getStringData(Global_Data.owner_driver_id).equals("")) {
                    _updateUserProfileInput.put("driver", _sharedPref.getStringData(Global_Data.id));

                } else {
                    _updateUserProfileInput.put("driver", _sharedPref.getStringData(Global_Data.id));

                }
                _updateUserProfileInput.put("owner", _sharedPref.getStringData(Global_Data.ass_owner_id));
                _updateUserProfileInput.put("vehicle", _sharedPref.getStringData(Global_Data.ass_vehicle_id));
                _updateUserProfileInput.put("campaign_details", JSONObject.NULL);
                String json2 = "";
                json2 = _updateUserProfileInput.toString();
                System.out.println("geo location input in background--   " + json2);
                JSONParser jsonParser = new JSONParser();
                _updateUserProfileOutputJSON = jsonParser.makeHttpRequest(Global_Data._url + "geolocation/", "POST", json2);
                System.out.println("Geo Location---   " + _updateUserProfileOutputJSON.getString("lattitude"));
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override

        protected void onPostExecute(String s) {
            super.onPostExecute(s);

            try {

            } catch (Exception e) {
                e.printStackTrace();
            }


        }
    }

    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(), currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }

    /**
     * Checks whether two providers are the same
     */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }
}
