package rodearr.app.com.rodearrapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Driver implements Serializable {
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("firstname")
    @Expose
    private String firstname;
    @SerializedName("lastname")
    @Expose
    private String lastname;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("contact_number")
    @Expose
    private String contactNumber;
    @SerializedName("dob")
    @Expose
    private String dob;
    @SerializedName("license_number")
    @Expose
    private String licenseNumber;
    @SerializedName("dl_img_front")
    @Expose
    private Object dlImgFront;
    @SerializedName("stats")
    @Expose
    private Stats stats;

    public Driver(Integer id, String firstname, String lastname, String gender, String contactNumber, String dob, String licenseNumber, Object dlImgFront, Stats stats) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.gender = gender;
        this.contactNumber = contactNumber;
        this.dob = dob;
        this.licenseNumber = licenseNumber;
        this.dlImgFront = dlImgFront;
        this.stats = stats;
    }
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getLicenseNumber() {
        return licenseNumber;
    }

    public void setLicenseNumber(String licenseNumber) {
        this.licenseNumber = licenseNumber;
    }

    public Object getDlImgFront() {
        return dlImgFront;
    }

    public void setDlImgFront(Object dlImgFront) {
        this.dlImgFront = dlImgFront;
    }

    public Stats getStats() {
        return stats;
    }

    public void setStats(Stats stats) {
        this.stats = stats;
    }
}
